﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Afn_WebReportes.Model
{
    class Dao
    {
        public ResponseCorreoMoviInt Asis_Respuesta_Correo_Movi_Int(EntryCorreoMoviInt entry)
        {
            ConnectionHelper connection = new ConnectionHelper();
            OracleConnection ObjConn = connection.OpenConnectionToDataBaseOracle("PERSONAS");
            List<ResponseCorreoMoviInt> lista = new List<ResponseCorreoMoviInt>();
            ResponseCorreoMoviInt entidad = null;
            if (ObjConn != null)
            {
                ObjConn.Open();
                OracleDataReader odr = null;
                //ResponseCorreoMoviInt entidad = null;
                OracleCommand command = ObjConn.CreateCommand();
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "SP_ASIS_RESPUESTA_CORREO_MOVI_INTERNO";
                command.Parameters.Add(new OracleParameter("ficha_sap", entry.FichaSap));
                command.Parameters.Add(new OracleParameter("codi_trab", entry.CodiTrab));
                command.Parameters.Add(new OracleParameter("codi_mov", entry.CodiMov));
                command.Parameters.Add(new OracleParameter("rpta", entry.Rpta));
                command.Parameters.Add(new OracleParameter("observaciones", entry.Observaciones));
                command.Parameters.Add(new OracleParameter("flujo", entry.Flujo));
                command.Parameters.Add(new OracleParameter("crsrtn", OracleDbType.RefCursor)).Direction = ParameterDirection.Output;
                odr = command.ExecuteReader();
                //lista = new List<ResponseCorreoMoviInt>();
                //while (odr.Read())
                //{
                //    entidad = new ResponseCorreoMoviInt
                //    {
                //        Valor = odr.IsDBNull(0) ? "" : odr.GetString(0),
                //        Mensaje = odr.IsDBNull(1) ? "" : odr.GetString(1),
                //        Html = odr.IsDBNull(2) ? "" : odr.GetString(2)
                //    };
                //    lista.Add(entidad);
                //}
                lista = new List<ResponseCorreoMoviInt>();
                while (odr.Read())
                {
                    entidad = new ResponseCorreoMoviInt
                    {
                        Valor = odr.IsDBNull(0) ? "" : odr.GetString(0),
                        Mensaje = odr.IsDBNull(1) ? "" : odr.GetString(1),
                        Html = odr.IsDBNull(2) ? "" : odr.GetString(2)
                    };
                }
                odr.Close();
                ObjConn.Close();
            }
            return entidad;
        }

        public ResponseSincronizacion InsertUpdate_TrabajadorToSiderPrd(Type_Trabajador Entry)
        {
            DataTable l_dt = null;
            ResponseSincronizacion Response = new ResponseSincronizacion
            {
                Status = -1,
                Message = "",
                Exception = ""
            };

            try
            {
                ConnectionHelper connection = new ConnectionHelper();

                using (SqlConnection conn = connection.OpenConnectionToDataBaseSqlServer("PERSONAS_SIDERPRD"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Ins_Upd_Trabajador_Sincronizacion";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.CommandTimeout = 720;
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@Trabajadores",
                        SqlDbType = SqlDbType.Structured,
                        Value = Entry.TableTrabajadores
                    });

                    da.Fill(l_dt);

                    foreach (DataRow Row in l_dt.Rows)
                    {
                        Response.Status = Row["Valor"].ToString() == "1000" ? 1 : -1;
                        Response.Exception = Row["Exception"].ToString();
                    }

                    return Response;
                }
            }
            catch (Exception e)
            {
                Response.Exception = e.Message;
                return Response;
            }
        }
        public ResponseSincronizacion InsertUpdate_UsuarioToSiderPrd(Type_Trabajador Entry)
        {
            DataTable l_dt = null;
            ResponseSincronizacion Response = new ResponseSincronizacion
            {
                Status = -1,
                Message = "",
                Exception = ""
            };

            try
            {
                ConnectionHelper connection = new ConnectionHelper();

                using (SqlConnection conn = connection.OpenConnectionToDataBaseSqlServer("Administracion"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Ins_Upd_Usuario_Sincronizacion";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.CommandTimeout = 720;
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@Trabajadores",
                        SqlDbType = SqlDbType.Structured,
                        Value = Entry.TableTrabajadores
                    });

                    da.Fill(l_dt);

                    foreach (DataRow Row in l_dt.Rows)
                    {
                        Response.Status = Row["Valor"].ToString() == "1000" ? 1 : -1;
                        Response.Exception = Row["Exception"].ToString();
                    }

                    return Response;
                }
            }
            catch (Exception e)
            {
                Response.Exception = e.Message;
                return Response;
            }
        }

        public ResponseSincronizacion InsertUpdate_GerenciasToSiderPrd(Type_Gerencias Entry)
        {
            DataTable l_dt = null;
            ResponseSincronizacion Response = new ResponseSincronizacion
            {
                Status = -1,
                Message = "",
                Exception = ""
            };

            try
            {
                ConnectionHelper connection = new ConnectionHelper();

                using (SqlConnection conn = connection.OpenConnectionToDataBaseSqlServer("PERSONAS_SIDERPRD"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Ins_Upd_Gerencias_Sincronizacion";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.CommandTimeout = 720;
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@Table",
                        SqlDbType = SqlDbType.Structured,
                        Value = Entry.Table
                    });

                    da.Fill(l_dt);

                    foreach (DataRow Row in l_dt.Rows)
                    {
                        Response.Status = Row["Valor"].ToString() == "1000" ? 1 : -1;
                        Response.Exception = Row["Exception"].ToString();
                    }

                    return Response;
                }
            }
            catch (Exception e)
            {
                Response.Exception = e.Message;
                return Response;
            }
        }
        public ResponseSincronizacion InsertUpdate_AreasToSiderPrd(Type_Areas Entry)
        {
            DataTable l_dt = null;
            ResponseSincronizacion Response = new ResponseSincronizacion
            {
                Status = -1,
                Message = "",
                Exception = ""
            };

            try
            {
                ConnectionHelper connection = new ConnectionHelper();

                using (SqlConnection conn = connection.OpenConnectionToDataBaseSqlServer("PERSONAS_SIDERPRD"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Ins_Upd_Areas_Sincronizacion";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.CommandTimeout = 720;
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@Table",
                        SqlDbType = SqlDbType.Structured,
                        Value = Entry.Table
                    });

                    da.Fill(l_dt);

                    foreach (DataRow Row in l_dt.Rows)
                    {
                        Response.Status = Row["Valor"].ToString() == "1000" ? 1 : -1;
                        Response.Exception = Row["Exception"].ToString();
                    }

                    return Response;
                }
            }
            catch (Exception e)
            {
                Response.Exception = e.Message;
                return Response;
            }
        }
        public ResponseSincronizacion InsertUpdate_SubareasToSiderPrd(Type_Subareas Entry)
        {
            DataTable l_dt = null;
            ResponseSincronizacion Response = new ResponseSincronizacion
            {
                Status = -1,
                Message = "",
                Exception = ""
            };

            try
            {
                ConnectionHelper connection = new ConnectionHelper();

                using (SqlConnection conn = connection.OpenConnectionToDataBaseSqlServer("PERSONAS_SIDERPRD"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Ins_Upd_Subareas_Sincronizacion";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.CommandTimeout = 720;
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@Table",
                        SqlDbType = SqlDbType.Structured,
                        Value = Entry.Table
                    });

                    da.Fill(l_dt);

                    foreach (DataRow Row in l_dt.Rows)
                    {
                        Response.Status = Row["Valor"].ToString() == "1000" ? 1 : -1;
                        Response.Exception = Row["Exception"].ToString();
                    }

                    return Response;
                }
            }
            catch (Exception e)
            {
                Response.Exception = e.Message;
                return Response;
            }
        }
        public ResponseSincronizacion InsertUpdate_PuestoTrabajoToSiderPrd(Type_PuestoTrabajo Entry)
        {
            DataTable l_dt = null;
            ResponseSincronizacion Response = new ResponseSincronizacion
            {
                Status = -1,
                Message = "",
                Exception = ""
            };

            try
            {
                ConnectionHelper connection = new ConnectionHelper();

                using (SqlConnection conn = connection.OpenConnectionToDataBaseSqlServer("PERSONAS_SIDERPRD"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Ins_Upd_PuestoTrabajo_Sincronizacion";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.CommandTimeout = 720;
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@Table",
                        SqlDbType = SqlDbType.Structured,
                        Value = Entry.Table
                    });

                    da.Fill(l_dt);

                    foreach (DataRow Row in l_dt.Rows)
                    {
                        Response.Status = Row["Valor"].ToString() == "1000" ? 1 : -1;
                        Response.Exception = Row["Exception"].ToString();
                    }

                    return Response;
                }
            }
            catch (Exception e)
            {
                Response.Exception = e.Message;
                return Response;
            }
        }
    }
}
