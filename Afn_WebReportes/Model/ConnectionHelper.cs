﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// Comentario Nilton zambrano
// Comentario Pedro Reque
// Comentario Pedro Reque v222
namespace Afn_WebReportes.Model
{
    class ConnectionHelper
    {
        public SqlConnection OpenConnectionToDataBaseSqlServer(string data_Base)
        {
            SqlConnection conexionSql = null;
            try
            {
                if (data_Base == "Administracion")
                {
                    conexionSql = new SqlConnection("Data Source=siderprd.database.windows.net;Initial Catalog=Administracion;Connect Timeout=30;User Id=superadminsider;Password=Sider1909;");
                } else if (data_Base == "Desarrollo")
                {
                    conexionSql = new SqlConnection("Data Source=ambientedesarrollo.database.windows.net;Initial Catalog=Administracion;Connect Timeout=30;User Id=SuperAdmin;Password=.Admin123.;");
                }
                else if (data_Base == "Biostar")
                {
                    conexionSql = new SqlConnection("Data Source=sqlsdpcp07;Initial Catalog=BioStar;Connect Timeout=30;User Id=sa;Password=sqlsdpcp07;");
                } else if (data_Base == "PERSONAS_SIDERPRD")
                {
                    conexionSql = new SqlConnection("Data Source=siderprd.database.windows.net;Initial Catalog=Personas;Connect Timeout=30;User Id=superadminsider;Password=Sider1909;");
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            return conexionSql;
        }
        public OracleConnection OpenConnectionToDataBaseOracle(string data_Base)
        {
            OracleConnection conexionOracle = null;
            try
            {
                if (data_Base == "PERSONAS")
                {
                    conexionOracle = new OracleConnection("Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=10.78.10.48)(PORT=1521)))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=prdacero.gerdau.net)));User Id=PERSONAS;Password=personas;");
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            return conexionOracle;
        }
    }
}
