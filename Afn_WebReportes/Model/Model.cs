﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Afn_WebReportes.Model
{
    public class ResponseCorreoMoviInt
    {
        public string Valor { get; set; }
        public string Mensaje { get; set; }
        public string Html { get; set; }
    }
    public class EntryCorreoMoviInt
    {
        public string Rpta { get; set; }
        public string CodiTrab { get; set; }
        public string CodiMov { get; set; }
        public string FichaSap { get; set; }
        public string Flujo { get; set; }
        public string Observaciones { get; set; }
    }

    public class EntryCorreoVacaciones
    {
        public int pn_codi_trab { get; set; }
        public int pn_codi_perfil { get; set; }
        public int pn_codi_pava { get; set; }
        public string pv_estado { get; set; }
    }

    public class ResponseCorreoVacaciones
    {
        public string status { get; set; }
        public string Message { get; set; }
    }
    public class ResponseSincronizacion
    {
        public int Status { get; set; }
        public string Message { get; set; }
        public string Exception { get; set; }
        public object Response { get; set; }
    }
    public class Trabajador_Model
    {
        public string CodiTrab { get; set; }
        public string FichaSap { get; set; }
        public string TarjetaHid { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Nombres { get; set; }
        public string FichaTareo { get; set; }
        public string FichaAntigua { get; set; }
        public string CodiGere { get; set; }
        public string CodiRela { get; set; }
        public string CodiMoco { get; set; }
        public string TipoEmpl { get; set; }
        public string CodiCeco { get; set; }
        public string CodiSede { get; set; }
        public string CodiPues { get; set; }
        public string CodiArea { get; set; }
        public string CodiSuba { get; set; }
        public string CodiCelu { get; set; }
        public string CodiSind { get; set; }
        public string CodiDepa { get; set; }
        public string CodiPrvi { get; set; }
        public string CodiDist { get; set; }
        public string CodiGruph { get; set; }
        public string FechaNac { get; set; }
        public string TipoDoc { get; set; }
        public string NroDoc { get; set; }
        public string Sexo { get; set; }
        public string Direccion { get; set; }
        public string CodiNied { get; set; }
        public string CodiEspe { get; set; }
        public string FechaIngreso { get; set; }
        public string FechaCese { get; set; }
        public string CodiEssalud { get; set; }
        public string CodiAfp { get; set; }
        public string GrupoSang { get; set; }
        public string TelefonoFijo { get; set; }
        public string TelefonoMovil { get; set; }
        public string FlagDescSusti { get; set; }
        public string FlagMovilidad { get; set; }
        public string Observacion { get; set; }
        public string Email { get; set; }
        public string UserRed { get; set; }
        public string NivelAprobWf { get; set; }
        public string CodiCome { get; set; }
        public string Jerarquia { get; set; }
        public string Foto { get; set; }
        public string FlagTarjCome { get; set; }
        public string CodiPusa { get; set; }
        public string Elegibles { get; set; }
        public string Estado { get; set; }
        public string SecuenciaHorario { get; set; }
        public string RpcEmpresa { get; set; }
        public string AnexoEmpresa { get; set; }
        public string CodiMorg { get; set; }
        public string NroCertificado { get; set; }
        public string CodiSeguAfp { get; set; }
    }
    public class Gerencias_Model
    {
        public string CodiGere { get; set; }
        public string Descripcion { get; set; }
        public string JefeGerencia { get; set; }
        public string Estado { get; set; }
        public string DescripcionCorta { get; set; }
        public string Color { get; set; }
    }
    public class Areas_Model
    {
        public string CodiArea { get; set; }
        public string CodiGere { get; set; }
        public string Descripcion { get; set; }
        public string JefeArea { get; set; }
        public string Estado { get; set; }
        public string DescripcionCorta { get; set; }
        public string Color { get; set; }
        public string CodiCeco { get; set; }
    }
    public class Subareas_Model
    {
        public string CodiSuba { get; set; }
        public string CodiArea { get; set; }
        public string Descripcion { get; set; }
        public string JefeSubarea { get; set; }
        public string Estado { get; set; }
        public string DescripcionCorta { get; set; }
        public string Color { get; set; }
    }
    public class PuestoTrabajo_Model
    {
        public string CodiPues { get; set; }
        public string Categoria { get; set; }
        public string Descripcion { get; set; }
        public string Estado { get; set; }
    }
    public class Type_Trabajador
    {
        public DataTable TableTrabajadores { get; set; }
    }
    public class Type_Gerencias
    {
        public DataTable Table { get; set; }
    }
    public class Type_Areas
    {
        public DataTable Table { get; set; }
    }
    public class Type_Subareas
    {
        public DataTable Table { get; set; }
    }
    public class Type_PuestoTrabajo
    {
        public DataTable Table { get; set; }
    }
}
