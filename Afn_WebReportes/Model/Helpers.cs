﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Afn_WebReportes.Model
{
    class Helpers
    {
        public static string executeAPI(string json_data, string url, string token)
        {
            var postString = json_data;
            byte[] data = UTF8Encoding.UTF8.GetBytes(postString);
            HttpWebRequest request;
            request = WebRequest.Create(url) as HttpWebRequest;

            request.Timeout = 10 * 10000;
            request.Method = "POST";
            request.ContentLength = data.Length;
            request.ContentType = "application/json; charset=utf-8";
            if (token != "")
            {
                request.Headers.Add("Authorization", token);
            }
            Stream postStream = request.GetRequestStream();
            postStream.Write(data, 0, data.Length);

            HttpWebResponse response = request.GetResponse() as HttpWebResponse;
            StreamReader reader = new StreamReader(response.GetResponseStream());
            return reader.ReadToEnd();
        }
        public static ResponseCorreoMoviInt getEnviarRespuesCorreoResponse(EntryCorreoMoviInt Entry)
        {

            var l_url = "";
            var my_jsondata = new
            {
                Username = Environment.GetEnvironmentVariable("USER_API_REPORTE_LOCAL"),
                Password = Environment.GetEnvironmentVariable("PASS_API_REPORTE_LOCAL")
            };
            //Tranform it to Json object
            string json_data = JsonConvert.SerializeObject(my_jsondata);
            // url de API Autentificacion
            l_url = Environment.GetEnvironmentVariable("URL_API_REPORTE_LOCAL_PRUEBA");

            //obtiene token de API CONSUMIDA
            string l_token = executeAPI(json_data, l_url + "/api/login/authenticate", "");
            l_token = l_token.Replace("\"", "");

            json_data = JsonConvert.SerializeObject(Entry);
            string l_return = executeAPI(json_data, l_url + "/api/Reportes/EnviarRespuestaCorreo", l_token);
            return JsonConvert.DeserializeObject<ResponseCorreoMoviInt>(l_return);
        }

        public static ResponseCorreoVacaciones getEnviarRespuesCorreoVacacionesResponse(string p1,string p2, string p3, string p4)
        {
            EntryCorreoVacaciones Entry = new EntryCorreoVacaciones();
            Entry.pv_estado = p1;
            Entry.pn_codi_trab = Convert.ToInt16(p2);
            Entry.pn_codi_perfil = Convert.ToInt16(p3);
            Entry.pn_codi_pava = Convert.ToInt16(p4);

            var l_url = "";
            var my_jsondata = new
            {
                Username = Environment.GetEnvironmentVariable("USER_API_REPORTE_LOCAL"),
                Password = Environment.GetEnvironmentVariable("PASS_API_REPORTE_LOCAL")
            };
            //Tranform it to Json object
            string json_data = JsonConvert.SerializeObject(my_jsondata);
            // url de API Autentificacion
            l_url = Environment.GetEnvironmentVariable("URL_API_REPORTE_LOCAL_PRUEBA");
            //l_url = "http://localhost:61580";
            //obtiene token de API CONSUMIDA
            string l_token = executeAPI(json_data, l_url + "/api/login/authenticate", "");
            l_token = l_token.Replace("\"", "");

            json_data = JsonConvert.SerializeObject(Entry);
            string l_return = executeAPI(json_data, l_url + "/api/Vacaciones/EnviarRespuestaCorreo", l_token);
            return JsonConvert.DeserializeObject<ResponseCorreoVacaciones>(l_return);
        }
    }
}
