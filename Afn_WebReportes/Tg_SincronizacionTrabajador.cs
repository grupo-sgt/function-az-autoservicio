using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using Afn_WebReportes.Model;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;

namespace Afn_WebReportes
{
    public static class Tg_SincronizacionTrabajador
    {
        [FunctionName("Tg_SincronizacionTrabajador")]
        public static void Run([TimerTrigger("0 0 5 * * *")]TimerInfo myTimer, TraceWriter log)
        {
            log.Info("------------ INICIO DE LA SINCRONIZACION -----------");

            log.Info("LISTAR TRABAJADORES");
            var _ListTrabajadores = ListarTrabajadores();
            log.Info("Cantidad de Trabajadores: " + _ListTrabajadores.Count());
            Type_Trabajador Trabajadores = new Type_Trabajador();
            DataRow _RowTrabajador = null;

            Trabajadores.TableTrabajadores = new DataTable();
            Trabajadores.TableTrabajadores.Columns.Add("CodiTrab");
            Trabajadores.TableTrabajadores.Columns.Add("FichaSap");
            Trabajadores.TableTrabajadores.Columns.Add("TarjetaHid");
            Trabajadores.TableTrabajadores.Columns.Add("ApellidoPaterno");
            Trabajadores.TableTrabajadores.Columns.Add("ApellidoMaterno");
            Trabajadores.TableTrabajadores.Columns.Add("Nombres");
            Trabajadores.TableTrabajadores.Columns.Add("FichaTareo");
            Trabajadores.TableTrabajadores.Columns.Add("FichaAntigua");
            Trabajadores.TableTrabajadores.Columns.Add("CodiGere");
            Trabajadores.TableTrabajadores.Columns.Add("CodiRela");
            Trabajadores.TableTrabajadores.Columns.Add("CodiMoco");
            Trabajadores.TableTrabajadores.Columns.Add("TipoEmpl");
            Trabajadores.TableTrabajadores.Columns.Add("CodiCeco");
            Trabajadores.TableTrabajadores.Columns.Add("CodiSede");
            Trabajadores.TableTrabajadores.Columns.Add("CodiPues");
            Trabajadores.TableTrabajadores.Columns.Add("CodiArea");
            Trabajadores.TableTrabajadores.Columns.Add("CodiSuba");
            Trabajadores.TableTrabajadores.Columns.Add("CodiCelu");
            Trabajadores.TableTrabajadores.Columns.Add("CodiSind");
            Trabajadores.TableTrabajadores.Columns.Add("CodiDepa");
            Trabajadores.TableTrabajadores.Columns.Add("CodiPrvi");
            Trabajadores.TableTrabajadores.Columns.Add("CodiDist");
            Trabajadores.TableTrabajadores.Columns.Add("CodiGruph");
            Trabajadores.TableTrabajadores.Columns.Add("FechaNac");
            Trabajadores.TableTrabajadores.Columns.Add("TipoDoc");
            Trabajadores.TableTrabajadores.Columns.Add("NroDoc");
            Trabajadores.TableTrabajadores.Columns.Add("Sexo");
            Trabajadores.TableTrabajadores.Columns.Add("Direccion");
            Trabajadores.TableTrabajadores.Columns.Add("CodiNied");
            Trabajadores.TableTrabajadores.Columns.Add("CodiEspe");
            Trabajadores.TableTrabajadores.Columns.Add("FechaIngreso");
            Trabajadores.TableTrabajadores.Columns.Add("FechaCese");
            Trabajadores.TableTrabajadores.Columns.Add("CodiEssalud");
            Trabajadores.TableTrabajadores.Columns.Add("CodiAfp");
            Trabajadores.TableTrabajadores.Columns.Add("GrupoSang");
            Trabajadores.TableTrabajadores.Columns.Add("TelefonoFijo");
            Trabajadores.TableTrabajadores.Columns.Add("TelefonoMovil");
            Trabajadores.TableTrabajadores.Columns.Add("FlagDescSusti");
            Trabajadores.TableTrabajadores.Columns.Add("FlagMovilidad");
            Trabajadores.TableTrabajadores.Columns.Add("Observacion");
            Trabajadores.TableTrabajadores.Columns.Add("Email");
            Trabajadores.TableTrabajadores.Columns.Add("UserRed");
            Trabajadores.TableTrabajadores.Columns.Add("NivelAprobWf");
            Trabajadores.TableTrabajadores.Columns.Add("CodiCome");
            Trabajadores.TableTrabajadores.Columns.Add("Jerarquia");
            Trabajadores.TableTrabajadores.Columns.Add("Foto");
            Trabajadores.TableTrabajadores.Columns.Add("FlagTarjCome");
            Trabajadores.TableTrabajadores.Columns.Add("CodiPusa");
            Trabajadores.TableTrabajadores.Columns.Add("Elegibles");
            Trabajadores.TableTrabajadores.Columns.Add("Estado");
            Trabajadores.TableTrabajadores.Columns.Add("SecuenciaHorario");
            Trabajadores.TableTrabajadores.Columns.Add("RpcEmpresa");
            Trabajadores.TableTrabajadores.Columns.Add("AnexoEmpresa");
            Trabajadores.TableTrabajadores.Columns.Add("CodiMorg");
            Trabajadores.TableTrabajadores.Columns.Add("NroCertificado");
            Trabajadores.TableTrabajadores.Columns.Add("CodiSeguAfp");

            foreach (Trabajador_Model Trabajador in _ListTrabajadores)
            {
                _RowTrabajador = Trabajadores.TableTrabajadores.NewRow();
                _RowTrabajador["CodiTrab"] = Trabajador.CodiTrab;
                _RowTrabajador["FichaSap"] = Trabajador.FichaSap;
                _RowTrabajador["TarjetaHid"] = Trabajador.TarjetaHid;
                _RowTrabajador["ApellidoPaterno"] = Trabajador.ApellidoPaterno;
                _RowTrabajador["ApellidoMaterno"] = Trabajador.ApellidoMaterno;
                _RowTrabajador["Nombres"] = Trabajador.Nombres;
                _RowTrabajador["FichaTareo"] = Trabajador.FichaTareo;
                _RowTrabajador["FichaAntigua"] = Trabajador.FichaAntigua;
                _RowTrabajador["CodiGere"] = Trabajador.CodiGere;
                _RowTrabajador["CodiRela"] = Trabajador.CodiRela;
                _RowTrabajador["CodiMoco"] = Trabajador.CodiMoco;
                _RowTrabajador["TipoEmpl"] = Trabajador.TipoEmpl;
                _RowTrabajador["CodiCeco"] = Trabajador.CodiCeco;
                _RowTrabajador["CodiSede"] = Trabajador.CodiSede;
                _RowTrabajador["CodiPues"] = Trabajador.CodiPues;
                _RowTrabajador["CodiArea"] = Trabajador.CodiArea;
                _RowTrabajador["CodiSuba"] = Trabajador.CodiSuba;
                _RowTrabajador["CodiCelu"] = Trabajador.CodiCelu;
                _RowTrabajador["CodiSind"] = Trabajador.CodiSind;
                _RowTrabajador["CodiDepa"] = Trabajador.CodiDepa;
                _RowTrabajador["CodiPrvi"] = Trabajador.CodiPrvi;
                _RowTrabajador["CodiDist"] = Trabajador.CodiDist;
                _RowTrabajador["CodiGruph"] = Trabajador.CodiGruph;
                _RowTrabajador["FechaNac"] = Trabajador.FechaNac;
                _RowTrabajador["TipoDoc"] = Trabajador.TipoDoc;
                _RowTrabajador["NroDoc"] = Trabajador.NroDoc;
                _RowTrabajador["Sexo"] = Trabajador.Sexo;
                _RowTrabajador["Direccion"] = Trabajador.Direccion;
                _RowTrabajador["CodiNied"] = Trabajador.CodiNied;
                _RowTrabajador["CodiEspe"] = Trabajador.CodiEspe;
                _RowTrabajador["FechaIngreso"] = Trabajador.FechaIngreso;
                _RowTrabajador["FechaCese"] = Trabajador.FechaCese;
                _RowTrabajador["CodiEssalud"] = Trabajador.CodiEssalud;
                _RowTrabajador["CodiAfp"] = Trabajador.CodiAfp;
                _RowTrabajador["GrupoSang"] = Trabajador.GrupoSang;
                _RowTrabajador["TelefonoFijo"] = Trabajador.TelefonoFijo;
                _RowTrabajador["TelefonoMovil"] = Trabajador.TelefonoMovil;
                _RowTrabajador["FlagDescSusti"] = Trabajador.FlagDescSusti;
                _RowTrabajador["FlagMovilidad"] = Trabajador.FlagMovilidad;
                _RowTrabajador["Observacion"] = Trabajador.Observacion;
                _RowTrabajador["Email"] = Trabajador.Email;
                _RowTrabajador["UserRed"] = Trabajador.UserRed;
                _RowTrabajador["NivelAprobWf"] = Trabajador.NivelAprobWf;
                _RowTrabajador["CodiCome"] = Trabajador.CodiCome;
                _RowTrabajador["Jerarquia"] = Trabajador.Jerarquia;
                _RowTrabajador["Foto"] = Trabajador.Foto;
                _RowTrabajador["FlagTarjCome"] = Trabajador.FlagTarjCome;
                _RowTrabajador["CodiPusa"] = Trabajador.CodiPusa;
                _RowTrabajador["Elegibles"] = Trabajador.Elegibles;
                _RowTrabajador["Estado"] = Trabajador.Estado;
                _RowTrabajador["SecuenciaHorario"] = Trabajador.SecuenciaHorario;
                _RowTrabajador["RpcEmpresa"] = Trabajador.RpcEmpresa;
                _RowTrabajador["AnexoEmpresa"] = Trabajador.AnexoEmpresa;
                _RowTrabajador["CodiMorg"] = Trabajador.CodiMorg;
                _RowTrabajador["NroCertificado"] = Trabajador.NroCertificado;
                _RowTrabajador["CodiSeguAfp"] = Trabajador.CodiSeguAfp;
                Trabajadores.TableTrabajadores.Rows.Add(_RowTrabajador);
                _RowTrabajador = null;
            }
            InsertUpdate_TrabajadorToSiderPrd(Trabajadores, log);
            InsertUpdate_TrabajadorToSiderPrd01(Trabajadores, log);
            InsertUpdate_TrabajadorToSql07(Trabajadores, log);

            log.Info("LISTAR GERENCIAS");
            var _ListGerencias = ListarGerencias(log);
            log.Info("Cantidad de Gerencias: " + _ListGerencias.Count());
            Type_Gerencias Gerencias = new Type_Gerencias();
            DataRow _RowGerencias = null;

            Gerencias.Table = new DataTable();
            Gerencias.Table.Columns.Add("CodiGere");
            Gerencias.Table.Columns.Add("Descripcion");
            Gerencias.Table.Columns.Add("JefeGerencia");
            Gerencias.Table.Columns.Add("Estado");
            Gerencias.Table.Columns.Add("DescripcionCorta");
            Gerencias.Table.Columns.Add("Color");

            foreach (Gerencias_Model Entity in _ListGerencias)
            {
                _RowGerencias = Gerencias.Table.NewRow();
                _RowGerencias["CodiGere"] = Entity.CodiGere;
                _RowGerencias["Descripcion"] = Entity.Descripcion;
                _RowGerencias["JefeGerencia"] = Entity.JefeGerencia;
                _RowGerencias["Estado"] = Entity.Estado;
                _RowGerencias["DescripcionCorta"] = Entity.DescripcionCorta;
                _RowGerencias["Color"] = Entity.Color;
                Gerencias.Table.Rows.Add(_RowGerencias);
                _RowGerencias = null;
            }
            InsertUpdate_GerenciasToSiderPrd(Gerencias, log);
            InsertUpdate_GerenciasToSiderPrd01(Gerencias, log);
            InsertUpdate_GerenciasToSql07(Gerencias, log);

            log.Info("LISTAR AREAS");
            var _ListAreas = ListarAreas(log);
            log.Info("Cantidad de Areas: " + _ListAreas.Count());
            Type_Areas Areas = new Type_Areas();
            DataRow _RowAreas = null;

            Areas.Table = new DataTable();
            Areas.Table.Columns.Add("CodiArea");
            Areas.Table.Columns.Add("CodiGere");
            Areas.Table.Columns.Add("Descripcion");
            Areas.Table.Columns.Add("JefeArea");
            Areas.Table.Columns.Add("Estado");
            Areas.Table.Columns.Add("DescripcionCorta");
            Areas.Table.Columns.Add("Color");
            Areas.Table.Columns.Add("CodiCeco");

            foreach (Areas_Model Entity in _ListAreas)
            {
                _RowAreas = Areas.Table.NewRow();
                _RowAreas["CodiArea"] = Entity.CodiArea;
                _RowAreas["CodiGere"] = Entity.CodiGere;
                _RowAreas["Descripcion"] = Entity.Descripcion;
                _RowAreas["JefeArea"] = Entity.JefeArea;
                _RowAreas["Estado"] = Entity.Estado;
                _RowAreas["DescripcionCorta"] = Entity.DescripcionCorta;
                _RowAreas["Color"] = Entity.Color;
                _RowAreas["CodiCeco"] = Entity.CodiCeco;
                Areas.Table.Rows.Add(_RowAreas);
                _RowAreas = null;
            }
            InsertUpdate_AreasToSiderPrd(Areas, log);
            InsertUpdate_AreasToSiderPrd01(Areas, log);
            InsertUpdate_AreasToSql07(Areas, log);

            log.Info("LISTAR SUBAREAS");
            var _ListSubareas = ListarSubareas(log);
            log.Info("Cantidad de Subareas: " + _ListSubareas.Count());
            Type_Subareas Subareas = new Type_Subareas();
            DataRow _RowSubreas = null;

            Subareas.Table = new DataTable();
            Subareas.Table.Columns.Add("CodiSuba");
            Subareas.Table.Columns.Add("CodiArea");
            Subareas.Table.Columns.Add("Descripcion");
            Subareas.Table.Columns.Add("JefeSubarea");
            Subareas.Table.Columns.Add("Estado");
            Subareas.Table.Columns.Add("DescripcionCorta");
            Subareas.Table.Columns.Add("Color");

            foreach (Subareas_Model Entity in _ListSubareas)
            {
                _RowSubreas = Subareas.Table.NewRow();
                _RowSubreas["CodiSuba"] = Entity.CodiSuba;
                _RowSubreas["CodiArea"] = Entity.CodiArea;
                _RowSubreas["Descripcion"] = Entity.Descripcion;
                _RowSubreas["JefeSubarea"] = Entity.JefeSubarea;
                _RowSubreas["Estado"] = Entity.Estado;
                _RowSubreas["DescripcionCorta"] = Entity.DescripcionCorta;
                _RowSubreas["Color"] = Entity.Color;
                Subareas.Table.Rows.Add(_RowSubreas);
                _RowSubreas = null;
            }
            InsertUpdate_SubareasToSiderPrd(Subareas, log);
            InsertUpdate_SubareasToSiderPrd01(Subareas, log);
            InsertUpdate_SubareasToSql07(Subareas, log);

            log.Info("LISTAR PUESTO DE TRABAJO");
            int _CountPuestoTrabajo = 0;
            int _CountPuestoTrabajoTotal = 0;
            int _CountPuestoTrabajoSiderPrd = 0;
            List<PuestoTrabajo_Model> _ListPuestoTrabajoAux = new List<PuestoTrabajo_Model>();
            var _ListPuestoTrabajo = ListarPuestoTrabajo(log);
            log.Info("Cantidad de Puesto de Trabajo: " + _ListPuestoTrabajo.Count());
            Type_PuestoTrabajo PuestoTrabajo = new Type_PuestoTrabajo();
            DataRow _RowPuestoTrabajo = null;

            PuestoTrabajo.Table = new DataTable();
            PuestoTrabajo.Table.Columns.Add("CodiPues");
            PuestoTrabajo.Table.Columns.Add("Categoria");
            PuestoTrabajo.Table.Columns.Add("Descripcion");
            PuestoTrabajo.Table.Columns.Add("Estado");

            foreach (PuestoTrabajo_Model Entity in _ListPuestoTrabajo)
            {
                _RowPuestoTrabajo = PuestoTrabajo.Table.NewRow();
                _RowPuestoTrabajo["CodiPues"] = Entity.CodiPues;
                _RowPuestoTrabajo["Categoria"] = Entity.Categoria;
                _RowPuestoTrabajo["Descripcion"] = Entity.Descripcion;
                _RowPuestoTrabajo["Estado"] = Entity.Estado;
                PuestoTrabajo.Table.Rows.Add(_RowPuestoTrabajo);
                _RowPuestoTrabajo = null;
            }
            InsertUpdate_PuestoTrabajoToSiderPrd(PuestoTrabajo, log);
            InsertUpdate_PuestoTrabajoToSiderPrd01(PuestoTrabajo, log);
            InsertUpdate_PuestoTrabajoToSql07(PuestoTrabajo, log);

            log.Info("------------ FIN DE LA SINCRONIZACION -----------");
        }
        private static List<Trabajador_Model> ListarTrabajadores()
        {
            var Login = new
            {
                Username = Environment.GetEnvironmentVariable("USER_API_REPORTE_LOCAL"),
                Password = Environment.GetEnvironmentVariable("PASS_API_REPORTE_LOCAL")
            };

            var Url = Environment.GetEnvironmentVariable("URL_API_REPORTE_LOCAL_PRUEBA");
            string Token = executeAPI(Login, Url + "/api/login/authenticate", "");
            Token = Token.Replace("\"", "");

            var Data = "";

            string StringResponse = executeAPI(Data, Url + "/api/SincronizacionTrabajador/ListarTrabajadores", Token);
            var JsonResponse = JsonConvert.DeserializeObject<ResponseSincronizacion>(StringResponse);

            if (JsonResponse.Status == 1)
            {
                return JsonConvert.DeserializeObject<List<Trabajador_Model>>(JsonResponse.Response.ToString());
            }
            else
            {
                return null;
            }
        }
        private static bool InsertUpdate_TrabajadorToSql07(Type_Trabajador Entry, TraceWriter log)
        {
            var Login = new
            {
                Username = Environment.GetEnvironmentVariable("USER_API_REPORTE_LOCAL"),
                Password = Environment.GetEnvironmentVariable("PASS_API_REPORTE_LOCAL")
            };

            var Url = Environment.GetEnvironmentVariable("URL_API_REPORTE_LOCAL_PRUEBA");
            string Token = executeAPI(Login, Url + "/api/login/authenticate", "");
            Token = Token.Replace("\"", "");

            var Data = Entry;

            string StringResponse = executeAPI(Data, Url + "/api/SincronizacionTrabajador/InsertUpdate_TrabajadorToSql07", Token);
            var JsonResponse = JsonConvert.DeserializeObject<ResponseSincronizacion>(StringResponse);

            if (JsonResponse.Status == 1)
            {
                if (JsonResponse.Exception != "")
                {
                    log.Info("Error InsertUpdate_TrabajadorToSql07: " + JsonResponse.Exception);
                }
                return true;
            }
            else
            {
                if (JsonResponse.Exception != "")
                {
                    log.Info("Error InsertUpdate_TrabajadorToSql07: " + JsonResponse.Exception);
                }
                return false;
            }
        }
        private static bool InsertUpdate_TrabajadorToSiderPrd01(Type_Trabajador Entry, TraceWriter log)
        {
            var Login = new
            {
                Username = Environment.GetEnvironmentVariable("USER_API_REPORTE_LOCAL"),
                Password = Environment.GetEnvironmentVariable("PASS_API_REPORTE_LOCAL")
            };

            var Url = Environment.GetEnvironmentVariable("URL_API_REPORTE_LOCAL_PRUEBA");
            string Token = executeAPI(Login, Url + "/api/login/authenticate", "");
            Token = Token.Replace("\"", "");

            var Data = Entry;

            string StringResponse = executeAPI(Data, Url + "/api/SincronizacionTrabajador/InsertUpdate_TrabajadorToSiderPrd01", Token);
            var JsonResponse = JsonConvert.DeserializeObject<ResponseSincronizacion>(StringResponse);

            if (JsonResponse.Status == 1)
            {
                if (JsonResponse.Exception != "")
                {
                    log.Info("Error InsertUpdate_TrabajadorToSiderPrd01: " + JsonResponse.Exception);
                }
                return true;
            }
            else
            {
                if (JsonResponse.Exception != "")
                {
                    log.Info("Error InsertUpdate_TrabajadorToSiderPrd01: " + JsonResponse.Exception);
                }
                return false;
            }
        }
        private static bool InsertUpdate_TrabajadorToSiderPrd(Type_Trabajador Entry, TraceWriter log)
        {
            Dao Dao = new Dao();
            var JsonResponse = Dao.InsertUpdate_TrabajadorToSiderPrd(Entry);

            if (JsonResponse.Status == 1)
            {
                if (JsonResponse.Exception != "")
                {
                    log.Info("Error InsertUpdate_TrabajadorToSiderPrd: " + JsonResponse.Exception);
                }

                var JsonResponseUsuario = Dao.InsertUpdate_UsuarioToSiderPrd(Entry);

                if (JsonResponseUsuario.Status == 1)
                {
                    if (JsonResponseUsuario.Exception != "")
                    {
                        log.Info("Error InsertUpdate_UsuarioToSiderPrd: " + JsonResponse.Exception);
                    }
                    return true;
                }
                else
                {
                    if (JsonResponseUsuario.Exception != "")
                    {
                        log.Info("Error InsertUpdate_UsuarioToSiderPrd: " + JsonResponse.Exception);
                    }
                    return false;
                }

            }
            else
            {
                if (JsonResponse.Exception != "")
                {
                    log.Info("Error InsertUpdate_TrabajadorToSiderPrd: " + JsonResponse.Exception);
                }
                return false;
            }
        }

        private static List<Gerencias_Model> ListarGerencias(TraceWriter log)
        {
            var Login = new
            {
                Username = Environment.GetEnvironmentVariable("USER_API_REPORTE_LOCAL"),
                Password = Environment.GetEnvironmentVariable("PASS_API_REPORTE_LOCAL")
            };

            var Url = Environment.GetEnvironmentVariable("URL_API_REPORTE_LOCAL_PRUEBA");
            string Token = executeAPI(Login, Url + "/api/login/authenticate", "");
            Token = Token.Replace("\"", "");

            var Data = "";

            string StringResponse = executeAPI(Data, Url + "/api/SincronizacionTrabajador/ListarGerencias", Token);
            var JsonResponse = JsonConvert.DeserializeObject<ResponseSincronizacion>(StringResponse);

            if (JsonResponse.Status == 1)
            {
                if (JsonResponse.Exception != "")
                {
                    log.Info("Error ListarGerencias: " + JsonResponse.Exception);
                }
                return JsonConvert.DeserializeObject<List<Gerencias_Model>>(JsonResponse.Response.ToString());
            }
            else
            {
                if (JsonResponse.Exception != "")
                {
                    log.Info("Error ListarGerencias: " + JsonResponse.Exception);
                }
                return null;
            }
        }
        private static List<Areas_Model> ListarAreas(TraceWriter log)
        {
            var Login = new
            {
                Username = Environment.GetEnvironmentVariable("USER_API_REPORTE_LOCAL"),
                Password = Environment.GetEnvironmentVariable("PASS_API_REPORTE_LOCAL")
            };

            var Url = Environment.GetEnvironmentVariable("URL_API_REPORTE_LOCAL_PRUEBA");
            string Token = executeAPI(Login, Url + "/api/login/authenticate", "");
            Token = Token.Replace("\"", "");

            var Data = "";

            string StringResponse = executeAPI(Data, Url + "/api/SincronizacionTrabajador/ListarAreas", Token);
            var JsonResponse = JsonConvert.DeserializeObject<ResponseSincronizacion>(StringResponse);

            if (JsonResponse.Status == 1)
            {
                if (JsonResponse.Exception != "")
                {
                    log.Info("Error ListarAreas: " + JsonResponse.Exception);
                }
                return JsonConvert.DeserializeObject<List<Areas_Model>>(JsonResponse.Response.ToString());
            }
            else
            {
                if (JsonResponse.Exception != "")
                {
                    log.Info("Error ListarAreas: " + JsonResponse.Exception);
                }
                return null;
            }
        }
        private static List<Subareas_Model> ListarSubareas(TraceWriter log)
        {
            var Login = new
            {
                Username = Environment.GetEnvironmentVariable("USER_API_REPORTE_LOCAL"),
                Password = Environment.GetEnvironmentVariable("PASS_API_REPORTE_LOCAL")
            };

            var Url = Environment.GetEnvironmentVariable("URL_API_REPORTE_LOCAL_PRUEBA");
            string Token = executeAPI(Login, Url + "/api/login/authenticate", "");
            Token = Token.Replace("\"", "");

            var Data = "";

            string StringResponse = executeAPI(Data, Url + "/api/SincronizacionTrabajador/ListarSubareas", Token);
            var JsonResponse = JsonConvert.DeserializeObject<ResponseSincronizacion>(StringResponse);

            if (JsonResponse.Status == 1)
            {
                if (JsonResponse.Exception != "")
                {
                    log.Info("Error ListarSubareas: " + JsonResponse.Exception);
                }
                return JsonConvert.DeserializeObject<List<Subareas_Model>>(JsonResponse.Response.ToString());
            }
            else
            {
                if (JsonResponse.Exception != "")
                {
                    log.Info("Error ListarSubareas: " + JsonResponse.Exception);
                }
                return null;
            }
        }
        private static List<PuestoTrabajo_Model> ListarPuestoTrabajo(TraceWriter log)
        {
            var Login = new
            {
                Username = Environment.GetEnvironmentVariable("USER_API_REPORTE_LOCAL"),
                Password = Environment.GetEnvironmentVariable("PASS_API_REPORTE_LOCAL")
            };

            var Url = Environment.GetEnvironmentVariable("URL_API_REPORTE_LOCAL_PRUEBA");
            string Token = executeAPI(Login, Url + "/api/login/authenticate", "");
            Token = Token.Replace("\"", "");

            var Data = "";

            string StringResponse = executeAPI(Data, Url + "/api/SincronizacionTrabajador/ListarPuestoTrabajo", Token);
            var JsonResponse = JsonConvert.DeserializeObject<ResponseSincronizacion>(StringResponse);

            if (JsonResponse.Status == 1)
            {
                if (JsonResponse.Exception != "")
                {
                    log.Info("Error ListarPuestoTrabajo: " + JsonResponse.Exception);
                }
                return JsonConvert.DeserializeObject<List<PuestoTrabajo_Model>>(JsonResponse.Response.ToString());
            }
            else
            {
                if (JsonResponse.Exception != "")
                {
                    log.Info("Error ListarPuestoTrabajo: " + JsonResponse.Exception);
                }
                return null;
            }
        }

        private static bool InsertUpdate_GerenciasToSql07(Type_Gerencias Entry, TraceWriter log)
        {
            var Login = new
            {
                Username = Environment.GetEnvironmentVariable("USER_API_REPORTE_LOCAL"),
                Password = Environment.GetEnvironmentVariable("PASS_API_REPORTE_LOCAL")
            };

            var Url = Environment.GetEnvironmentVariable("URL_API_REPORTE_LOCAL_PRUEBA");
            string Token = executeAPI(Login, Url + "/api/login/authenticate", "");
            Token = Token.Replace("\"", "");

            var Data = Entry;

            string StringResponse = executeAPI(Data, Url + "/api/SincronizacionTrabajador/InsertUpdate_GerenciasToSql07", Token);
            var JsonResponse = JsonConvert.DeserializeObject<ResponseSincronizacion>(StringResponse);

            if (JsonResponse.Status == 1)
            {
                if (JsonResponse.Exception != "")
                {
                    log.Info("Error InsertUpdate_GerenciasToSql07: " + JsonResponse.Exception);
                }
                return true;
            }
            else
            {
                if (JsonResponse.Exception != "")
                {
                    log.Info("Error InsertUpdate_GerenciasToSql07: " + JsonResponse.Exception);
                }
                return false;
            }
        }
        private static bool InsertUpdate_AreasToSql07(Type_Areas Entry, TraceWriter log)
        {
            var Login = new
            {
                Username = Environment.GetEnvironmentVariable("USER_API_REPORTE_LOCAL"),
                Password = Environment.GetEnvironmentVariable("PASS_API_REPORTE_LOCAL")
            };

            var Url = Environment.GetEnvironmentVariable("URL_API_REPORTE_LOCAL_PRUEBA");
            string Token = executeAPI(Login, Url + "/api/login/authenticate", "");
            Token = Token.Replace("\"", "");

            var Data = Entry;

            string StringResponse = executeAPI(Data, Url + "/api/SincronizacionTrabajador/InsertUpdate_AreasToSql07", Token);
            var JsonResponse = JsonConvert.DeserializeObject<ResponseSincronizacion>(StringResponse);

            if (JsonResponse.Status == 1)
            {
                if (JsonResponse.Exception != "")
                {
                    log.Info("Error InsertUpdate_AreasToSql07: " + JsonResponse.Exception);
                }
                return true;
            }
            else
            {
                if (JsonResponse.Exception != "")
                {
                    log.Info("Error InsertUpdate_AreasToSql07: " + JsonResponse.Exception);
                }
                return false;
            }
        }
        private static bool InsertUpdate_SubareasToSql07(Type_Subareas Entry, TraceWriter log)
        {
            var Login = new
            {
                Username = Environment.GetEnvironmentVariable("USER_API_REPORTE_LOCAL"),
                Password = Environment.GetEnvironmentVariable("PASS_API_REPORTE_LOCAL")
            };

            var Url = Environment.GetEnvironmentVariable("URL_API_REPORTE_LOCAL_PRUEBA");
            string Token = executeAPI(Login, Url + "/api/login/authenticate", "");
            Token = Token.Replace("\"", "");

            var Data = Entry;

            string StringResponse = executeAPI(Data, Url + "/api/SincronizacionTrabajador/InsertUpdate_SubareasToSql07", Token);
            var JsonResponse = JsonConvert.DeserializeObject<ResponseSincronizacion>(StringResponse);

            if (JsonResponse.Status == 1)
            {
                if (JsonResponse.Exception != "")
                {
                    log.Info("Error InsertUpdate_SubareasToSql07: " + JsonResponse.Exception);
                }
                return true;
            }
            else
            {
                if (JsonResponse.Exception != "")
                {
                    log.Info("Error InsertUpdate_SubareasToSql07: " + JsonResponse.Exception);
                }
                return false;
            }
        }
        private static bool InsertUpdate_PuestoTrabajoToSql07(Type_PuestoTrabajo Entry, TraceWriter log)
        {
            var Login = new
            {
                Username = Environment.GetEnvironmentVariable("USER_API_REPORTE_LOCAL"),
                Password = Environment.GetEnvironmentVariable("PASS_API_REPORTE_LOCAL")
            };

            var Url = Environment.GetEnvironmentVariable("URL_API_REPORTE_LOCAL_PRUEBA");
            string Token = executeAPI(Login, Url + "/api/login/authenticate", "");
            Token = Token.Replace("\"", "");

            var Data = Entry;

            string StringResponse = executeAPI(Data, Url + "/api/SincronizacionTrabajador/InsertUpdate_PuestoTrabajoToSql07", Token);
            var JsonResponse = JsonConvert.DeserializeObject<ResponseSincronizacion>(StringResponse);

            if (JsonResponse.Status == 1)
            {
                if (JsonResponse.Exception != "")
                {
                    log.Info("Error InsertUpdate_PuestoTrabajoToSql07: " + JsonResponse.Exception);
                }
                return true;
            }
            else
            {
                if (JsonResponse.Exception != "")
                {
                    log.Info("Error InsertUpdate_PuestoTrabajoToSql07: " + JsonResponse.Exception);
                }
                return false;
            }
        }

        private static bool InsertUpdate_GerenciasToSiderPrd01(Type_Gerencias Entry, TraceWriter log)
        {
            var Login = new
            {
                Username = Environment.GetEnvironmentVariable("USER_API_REPORTE_LOCAL"),
                Password = Environment.GetEnvironmentVariable("PASS_API_REPORTE_LOCAL")
            };

            var Url = Environment.GetEnvironmentVariable("URL_API_REPORTE_LOCAL_PRUEBA");
            string Token = executeAPI(Login, Url + "/api/login/authenticate", "");
            Token = Token.Replace("\"", "");

            var Data = Entry;

            string StringResponse = executeAPI(Data, Url + "/api/SincronizacionTrabajador/InsertUpdate_GerenciasToSiderPrd01", Token);
            var JsonResponse = JsonConvert.DeserializeObject<ResponseSincronizacion>(StringResponse);

            if (JsonResponse.Status == 1)
            {
                if (JsonResponse.Exception != "")
                {
                    log.Info("Error InsertUpdate_GerenciasToSiderPrd01: " + JsonResponse.Exception);
                }
                return true;
            }
            else
            {
                if (JsonResponse.Exception != "")
                {
                    log.Info("Error InsertUpdate_GerenciasToSiderPrd01: " + JsonResponse.Exception);
                }
                return false;
            }
        }
        private static bool InsertUpdate_AreasToSiderPrd01(Type_Areas Entry, TraceWriter log)
        {
            var Login = new
            {
                Username = Environment.GetEnvironmentVariable("USER_API_REPORTE_LOCAL"),
                Password = Environment.GetEnvironmentVariable("PASS_API_REPORTE_LOCAL")
            };

            var Url = Environment.GetEnvironmentVariable("URL_API_REPORTE_LOCAL_PRUEBA");
            string Token = executeAPI(Login, Url + "/api/login/authenticate", "");
            Token = Token.Replace("\"", "");

            var Data = Entry;

            string StringResponse = executeAPI(Data, Url + "/api/SincronizacionTrabajador/InsertUpdate_AreasToSiderPrd01", Token);
            var JsonResponse = JsonConvert.DeserializeObject<ResponseSincronizacion>(StringResponse);

            if (JsonResponse.Status == 1)
            {
                if (JsonResponse.Exception != "")
                {
                    log.Info("Error InsertUpdate_AreasToSiderPrd01: " + JsonResponse.Exception);
                }
                return true;
            }
            else
            {
                if (JsonResponse.Exception != "")
                {
                    log.Info("Error InsertUpdate_AreasToSiderPrd01: " + JsonResponse.Exception);
                }
                return false;
            }
        }
        private static bool InsertUpdate_SubareasToSiderPrd01(Type_Subareas Entry, TraceWriter log)
        {
            var Login = new
            {
                Username = Environment.GetEnvironmentVariable("USER_API_REPORTE_LOCAL"),
                Password = Environment.GetEnvironmentVariable("PASS_API_REPORTE_LOCAL")
            };

            var Url = Environment.GetEnvironmentVariable("URL_API_REPORTE_LOCAL_PRUEBA");
            string Token = executeAPI(Login, Url + "/api/login/authenticate", "");
            Token = Token.Replace("\"", "");

            var Data = Entry;

            string StringResponse = executeAPI(Data, Url + "/api/SincronizacionTrabajador/InsertUpdate_SubareasToSiderPrd01", Token);
            var JsonResponse = JsonConvert.DeserializeObject<ResponseSincronizacion>(StringResponse);

            if (JsonResponse.Status == 1)
            {
                if (JsonResponse.Exception != "")
                {
                    log.Info("Error InsertUpdate_SubareasToSiderPrd01: " + JsonResponse.Exception);
                }
                return true;
            }
            else
            {
                if (JsonResponse.Exception != "")
                {
                    log.Info("Error InsertUpdate_SubareasToSiderPrd01: " + JsonResponse.Exception);
                }
                return false;
            }
        }
        private static bool InsertUpdate_PuestoTrabajoToSiderPrd01(Type_PuestoTrabajo Entry, TraceWriter log)
        {
            var Login = new
            {
                Username = Environment.GetEnvironmentVariable("USER_API_REPORTE_LOCAL"),
                Password = Environment.GetEnvironmentVariable("PASS_API_REPORTE_LOCAL")
            };

            var Url = Environment.GetEnvironmentVariable("URL_API_REPORTE_LOCAL_PRUEBA");
            string Token = executeAPI(Login, Url + "/api/login/authenticate", "");
            Token = Token.Replace("\"", "");

            var Data = Entry;

            string StringResponse = executeAPI(Data, Url + "/api/SincronizacionTrabajador/InsertUpdate_PuestoTrabajoToSiderPrd01", Token);
            var JsonResponse = JsonConvert.DeserializeObject<ResponseSincronizacion>(StringResponse);

            if (JsonResponse.Status == 1)
            {
                if (JsonResponse.Exception != "")
                {
                    log.Info("Error InsertUpdate_PuestoTrabajoToSiderPrd01: " + JsonResponse.Exception);
                }
                return true;
            }
            else
            {
                if (JsonResponse.Exception != "")
                {
                    log.Info("Error InsertUpdate_PuestoTrabajoToSiderPrd01: " + JsonResponse.Exception);
                }
                return false;
            }
        }

        private static bool InsertUpdate_GerenciasToSiderPrd(Type_Gerencias Entry, TraceWriter log)
        {
            Dao Dao = new Dao();

            var JsonResponse = Dao.InsertUpdate_GerenciasToSiderPrd(Entry);

            if (JsonResponse.Status == 1)
            {
                if (JsonResponse.Exception != "")
                {
                    log.Info("Error InsertUpdate_GerenciasToSiderPrd: " + JsonResponse.Exception);
                }
                return true;
            }
            else
            {
                if (JsonResponse.Exception != "")
                {
                    log.Info("Error InsertUpdate_GerenciasToSiderPrd: " + JsonResponse.Exception);
                }
                return false;
            }
        }
        private static bool InsertUpdate_AreasToSiderPrd(Type_Areas Entry, TraceWriter log)
        {
            Dao Dao = new Dao();

            var JsonResponse = Dao.InsertUpdate_AreasToSiderPrd(Entry);

            if (JsonResponse.Status == 1)
            {
                if (JsonResponse.Exception != "")
                {
                    log.Info("Error InsertUpdate_AreasToSiderPrd: " + JsonResponse.Exception);
                }
                return true;
            }
            else
            {
                if (JsonResponse.Exception != "")
                {
                    log.Info("Error InsertUpdate_AreasToSiderPrd: " + JsonResponse.Exception);
                }
                return false;
            }
        }
        private static bool InsertUpdate_SubareasToSiderPrd(Type_Subareas Entry, TraceWriter log)
        {
            Dao Dao = new Dao();

            var JsonResponse = Dao.InsertUpdate_SubareasToSiderPrd(Entry);

            if (JsonResponse.Status == 1)
            {
                if (JsonResponse.Exception != "")
                {
                    log.Info("Error InsertUpdate_SubareasToSiderPrd: " + JsonResponse.Exception);
                }
                return true;
            }
            else
            {
                if (JsonResponse.Exception != "")
                {
                    log.Info("Error InsertUpdate_SubareasToSiderPrd: " + JsonResponse.Exception);
                }
                return false;
            }
        }
        private static bool InsertUpdate_PuestoTrabajoToSiderPrd(Type_PuestoTrabajo Entry, TraceWriter log)
        {
            Dao Dao = new Dao();

            var JsonResponse = Dao.InsertUpdate_PuestoTrabajoToSiderPrd(Entry);

            if (JsonResponse.Status == 1)
            {
                if (JsonResponse.Exception != "")
                {
                    log.Info("Error InsertUpdate_PuestoTrabajoToSiderPrd: " + JsonResponse.Exception);
                }
                return true;
            }
            else
            {
                if (JsonResponse.Exception != "")
                {
                    log.Info("Error InsertUpdate_PuestoTrabajoToSiderPrd: " + JsonResponse.Exception);
                }
                return false;
            }
        }

        public static string executeAPI(object json_data, string url, string token)
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            using (var client = new HttpClient())
            {
                if (token != "")
                {
                    client.DefaultRequestHeaders.Add("authorization", token);
                }
                client.BaseAddress = new Uri(url);
                //HTTP POST
                var postTask = client.PostAsJsonAsync("", json_data);
                postTask.Wait();

                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return result.Content.ReadAsStringAsync().Result;
                }
                else
                {
                    return "ERROR";
                }
            }
        }
    }
}
