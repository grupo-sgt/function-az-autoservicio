using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Afn_WebReportes.Model;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;

namespace Afn_WebReportes
{
    public static class Http_EnviarRespuestaVacacion
    {
        [FunctionName("Http_EnviarRespuestaVacacion")]
        public static async Task<HttpResponseMessage> Run([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            log.Info("C# HTTP trigger function processed a request.");
            log.Info("url:" + req.RequestUri.OriginalString);

            try
            {
                // parse query parameter
                string rpta = req.GetQueryNameValuePairs()
                    .FirstOrDefault(q => string.Compare(q.Key, "Rpta", true) == 0)
                    .Value;
                string codi_trab = req.GetQueryNameValuePairs()
                    .FirstOrDefault(q => string.Compare(q.Key, "CodiTrab", true) == 0)
                    .Value;
                string codi_perf = req.GetQueryNameValuePairs()
                    .FirstOrDefault(q => string.Compare(q.Key, "CodiPerf", true) == 0)
                    .Value;
                string codi_pava = req.GetQueryNameValuePairs()
                    .FirstOrDefault(q => string.Compare(q.Key, "CodiPava", true) == 0)
                    .Value;

                if (rpta == null)
                {
                    string l_url = req.RequestUri.OriginalString;

                    string[] data_url = l_url.Split('=');

                    rpta = data_url[data_url.Length - 4];
                    codi_trab = data_url[data_url.Length - 3];
                    codi_perf = data_url[data_url.Length - 2];
                    codi_pava = data_url[data_url.Length - 1];
                }

                if (rpta == null)
                {
                    // Get request body
                    dynamic data = await req.Content.ReadAsAsync<object>();
                    rpta = data?.rpta;
                }

                ResponseCorreoVacaciones l_return = Helpers.getEnviarRespuesCorreoVacacionesResponse(rpta, codi_trab, codi_perf, codi_pava);

                var response = new HttpResponseMessage();
                response.Content = new StringContent(l_return.Message);
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
                //var name = "sss";
                //return name == null
                //    ? req.CreateResponse(HttpStatusCode.BadRequest, "Please pass a name on the query string or in the request body")
                //    : req.CreateResponse(HttpStatusCode.OK, "Hello " + name);
                return response;
            }
            catch (Exception e)
            {
                return req.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }
    }
}
