using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Afn_WebReportes.Model;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;

namespace Afn_WebReportes
{
    public static class EnviarRespuestaMovimientoInterno
    {
        [FunctionName("EnviarRespuestaMovimientoInterno")]
        public static async Task<HttpResponseMessage> Run([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            log.Info("C# HTTP trigger function processed a request.");

            // parse query parameter
            string rpta = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "Rpta", true) == 0)
                .Value;
            string ficha_sap = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "FichaSap", true) == 0)
                .Value;
            string codi_trab = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "CodiTrab", true) == 0)
                .Value;
            string codi_mov = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "CodiMov", true) == 0)
                .Value;
            string flujo = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "Flujo", true) == 0)
                .Value;
            string observaciones = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "Observaciones", true) == 0)
                .Value;

            if (rpta == null)
            {
                // Get request body
                dynamic data = await req.Content.ReadAsAsync<object>();
                rpta = data?.rpta;
            }

            EntryCorreoMoviInt Entry = new EntryCorreoMoviInt();
            Entry.Rpta = rpta;
            Entry.FichaSap = ficha_sap;
            Entry.CodiTrab = codi_trab;
            Entry.CodiMov = codi_mov;
            Entry.Flujo = flujo;
            Entry.Observaciones = observaciones;
            
            ResponseCorreoMoviInt l_return = Helpers.getEnviarRespuesCorreoResponse(Entry);

            var response = new HttpResponseMessage();
            response.Content = new StringContent(l_return.Html);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");

            //return name == null
            //    ? req.CreateResponse(HttpStatusCode.BadRequest, "Please pass a name on the query string or in the request body")
            //    : req.CreateResponse(HttpStatusCode.OK, "Hello " + name);
            return response;
        }
    }
}
